module WordleSolve where

import TrieSet

import Flow
import qualified Data.Set as S

data Result = Grey | Yellow | Green

isGrey, isYellow, isGreen :: Result -> Bool
isGrey Grey = True
isGrey _    = False
isYellow Yellow = True
isYellow _      = False
isGreen Green = True
isGreen _     = False

update' :: String -> TrieSet Char -> TrieSet Char
update' [c1,r1,c2,r2,c3,r3,c4,r4,c5,r5]
  = update [(c1,f r1),(c2,f r2),(c3,f r3),(c4,f r4),(c5,f r5)]
  where
    f 'X' = Grey
    f 'Y' = Yellow
    f 'G' = Green
    f _   = error "cba to properly parse this"
update' _ = error "just do it right"

update :: [(Char, Result)] -> TrieSet Char -> TrieSet Char
update results
  = applyAll (greens  |> map (uncurry fixAtDepth))
 .> applyAll (yellows |> map (uncurry deleteAtDepth))
 .> deleteKeys greySet
 .> applyAll (yellows |> map (fst .> requireKeyHardRemove))
  -- = _ [ commands c r i
  --   | ((c,r), i) <- zip results [1..5]
  --   ]
  where
    -- commands :: Char -> Result -> Int -> [TrieSet Char -> TrieSet Char]
    -- commands c r i
    --   = case r of
    --   Grey   -> 
    --   Yellow -> _
    --   Green -> _
    

    greySet = results
           |> filter (snd .> isGrey)
           |> map fst
           |> S.fromList
    
    yellows :: [(Char, Int)]
    yellows = results
           |> zip [1..5]
           |> filter (snd .> snd .> isYellow)
           |> map (\(i, (c, _)) -> (c, i))

    greens :: [(Char, Int)]
    greens = results
          |> zip [1..5]
          |> filter (snd .> snd .> isGreen)
          |> map (\(i, (c, _)) -> (c, i))

-- >>> applyAll [(+2), (*10)] 1
-- 12

applyAll :: [a -> a] -> a -> a
applyAll fs x = foldr ($) x fs

exampleTrie :: TrieSet Char
exampleTrie = insertAll exampleList empty

exampleList :: [String]
exampleList
  = [
    "cigar",
    "rebut",
    "sissy",
    "humph",
    "awake",
    "blush",
    "focal",
    "evade",
    "naval",
    "serve",
    "heath",
    "dwarf",
    "model",
    "karma",
    "stink",
    "grade"
  ]

-- import WordsTrieSet

-- elimChar :: Char -> TrieSet Char -> TrieSet Char
-- elimChar c (MkTrie tag m)
--   = 
