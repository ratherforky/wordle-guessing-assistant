{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE RankNTypes #-}
{-# language StrictData, ScopedTypeVariables, DeriveLift, DeriveGeneric, DeriveAnyClass #-}
module TrieSet where

import Data.Hashable ( Hashable )
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as M
import Data.Tree ( Tree(Node), drawTree, drawForest )
import Flow
import Data.List (foldl')

-- import Bastian.DrawingTrees (Tree(..), design)
import Language.Haskell.TH
import Language.Haskell.TH.Syntax
import Instances.TH.Lift
import Control.DeepSeq
import GHC.Generics
import qualified Data.Set as S
import Data.Functor.Identity

data TrieSet k = MkTrie Tag (Map k (TrieSet k))
  deriving (Show, Lift, Generic, NFData)

data Tag = Member | NonMember
  deriving (Show, Lift, Generic, NFData)

drawTrie :: TrieSet Char -> String
drawTrie tset
  = toTree ('.', tset)
 |> fmap pretty
 |> drawTree
  where
    toTree :: (k, TrieSet k) -> Tree (k, Tag)
    toTree (key, MkTrie tag m)
      = M.toAscList m
     |> map toTree
     |> Node (key, tag)
    
    pretty :: (Char, Tag) -> String
    pretty (c, tag) = case tag of
      Member    -> ['<', c, '>']
      NonMember -> [c]


isMember :: Tag -> Bool
isMember Member = True
isMember NonMember = False

testTrie :: TrieSet Char
testTrie
  = newFromKey "greg"
 |> insert "grig"
 |> insert "grim"

empty :: TrieSet k
empty = MkTrie NonMember M.empty

newFromKey :: Ord k => [k] -> TrieSet k
newFromKey []     = MkTrie Member M.empty
newFromKey (k:ks) = MkTrie NonMember (M.singleton k (newFromKey ks))

insert :: forall k. Ord k => [k] -> TrieSet k -> TrieSet k
insert [] (MkTrie tag m) = MkTrie Member m
insert (k:ks) (MkTrie tag m) = MkTrie tag (M.alter f k m)
  -- case maybeMap of
  -- Nothing -> newFromKey (k:ks)
  -- Just mm -> MkTrie (Just $ M.alter f k mm)
  where
    f :: Maybe (TrieSet k) -> Maybe (TrieSet k)
    f Nothing     = Just $ newFromKey ks
    f (Just tset) = Just $ insert ks tset

insertAll :: Ord k => [[k]] -> TrieSet k -> TrieSet k
insertAll keyss tset = foldl' (flip insert) tset keyss

member :: Ord k => [k] -> TrieSet k -> Bool
member [] (MkTrie tag _) = isMember tag
member (k:ks) (MkTrie _ m) = case M.lookup k m of 
  Nothing -> False
  Just ts -> member ks ts

-- deleteAll :: Ord k => k -> TrieSet k -> TrieSet k
-- deleteAll key (MkTrie tag m)
--   = M.delete key m
--  |> fmap (deleteAll key)
--  |> MkTrie tag

deleteKeys :: Ord k => S.Set k -> TrieSet k -> TrieSet k
deleteKeys set = mapMapRec (`M.withoutKeys` set)

mapMap :: (Map k (TrieSet k) -> Map k (TrieSet k)) -> (TrieSet k -> TrieSet k)
mapMap f (MkTrie tag m) = MkTrie tag (f m)

-- Existential a to ensure function only works on one level
mapMapRec :: (forall a. Map k a -> Map k a) -> (TrieSet k -> TrieSet k)
mapMapRec f (MkTrie tag m) = MkTrie tag (f (fmap (mapMapRec f) m))

mapMapWithDepth :: forall k. (Int -> Map k (TrieSet k) -> Map k (TrieSet k)) -> (TrieSet k -> TrieSet k)
mapMapWithDepth f = go 1
  where
    go :: Int -> TrieSet k -> TrieSet k
    go depth (MkTrie tag m) = MkTrie tag (f depth (fmap (go (depth+1)) m))

-- 1-based
modifyAtDepth :: Int -> (Map k (TrieSet k) -> Map k (TrieSet k)) -> (TrieSet k -> TrieSet k)
modifyAtDepth n f = mapMapWithDepth (\depth -> if depth == n then f else id)
-- modifyAtDepth :: Int -> (TrieSet k -> TrieSet k) -> (TrieSet k -> TrieSet k)
-- modifyAtDepth n f tset
--   | n < 1     = tset
--   | n == 1    = f tset
--   | otherwise = case tset of
--     MkTrie tag m -> MkTrie tag (fmap (modifyAtDepth (n-1) f) m)

fixAtDepth :: Ord k => k -> Int -> TrieSet k -> TrieSet k
fixAtDepth key depth = modifyAtDepth depth (`M.restrictKeys` S.singleton key)

deleteAtDepth :: Ord k => k -> Int -> TrieSet k -> TrieSet k
deleteAtDepth key depth = modifyAtDepth depth (M.delete key)

requireKey :: forall k. Ord k => k -> TrieSet k -> TrieSet k
requireKey key = go
  where
    -- If go reaches a node, it has not contained the key up to that point
    -- so it is set to be NonMember no matter what
    go :: TrieSet k -> TrieSet k
    go (MkTrie tag m)
      = MkTrie NonMember
      $ M.mapWithKey (\k tset -> if k == key
                                 then tset -- Key present, so leave leaves untouched
                                 else go tset -- Key has not been found yet, so continue untagging
                     ) m

-- Prob most expensive operation
requireKeyHardRemove :: forall k. Ord k => k -> TrieSet k -> TrieSet k
requireKeyHardRemove key tset = case go tset of
  Nothing -> empty
  Just ts -> ts
  where
    -- If go reaches a node, it has not contained the key up to that point
    -- so it is set to be NonMember no matter what
    go :: TrieSet k -> Maybe (TrieSet k)
    go (MkTrie tag m)
      | M.null m = Nothing
      | M.null prunedFork = Nothing
      | otherwise = Just (MkTrie NonMember prunedFork)
      where
        prunedFork = mapWithKeyKeepJusts foo m

        mapWithKeyKeepJusts :: (k -> a -> Maybe b) -> Map k a -> Map k b
        mapWithKeyKeepJusts f
          = M.traverseMaybeWithKey (\key x -> Identity $ f key x)
         .> runIdentity

        foo :: k -> TrieSet k -> Maybe (TrieSet k)
        foo k tset' 
          | k == key  = Just tset' -- Key present, so leave leaves untouched
          | otherwise = go tset'   -- Key has not been found yet, so continue untagging
                       

setNonMember :: TrieSet k -> TrieSet k
setNonMember (MkTrie _ m) = MkTrie NonMember m

toList :: TrieSet k -> [[k]]
toList = go []
  where
    go :: [k] -> TrieSet k -> [[k]]
    go ks (MkTrie tag m) = case tag of
      NonMember -> recurse
      Member    -> reverse ks : recurse
      where
        recurse = concatMap (\(k, tset) -> go (k:ks) tset) $ M.toList m
