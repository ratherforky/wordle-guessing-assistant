{-# language TemplateHaskell, Strict #-}
module WordsTrieSet where

import TrieSet
import Words

import Control.DeepSeq
import Language.Haskell.TH
import Language.Haskell.TH.Syntax

answersTrie :: TrieSet Char
answersTrie = $$(liftTyped (force (insertAll answersList empty)))

guessesTrie :: TrieSet Char
guessesTrie =  $$(liftTyped (force (insertAll guessesList empty)))
