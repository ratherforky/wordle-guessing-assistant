module Main where

import WordleSolve
import WordsTrieSet
import TrieSet

main :: IO ()
main = narrow answersTrie

narrow :: TrieSet Char -> IO ()
narrow tset = do
  putStrLn "Enter guess result:"
  line <- getLine
  let refinedTset = update' line tset
  putStr "Remaining possible answers: "
  print (toList refinedTset)
  narrow refinedTset
